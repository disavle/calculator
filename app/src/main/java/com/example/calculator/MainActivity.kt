package com.example.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.EditText
import android.widget.TextView
import net.objecthunter.exp4j.ExpressionBuilder

class MainActivity : AppCompatActivity() {

    private lateinit var signView: TextView
    private lateinit var resultView: TextView
    private lateinit var inputView: EditText
    private lateinit var clearView: TextView
    private lateinit var inputSting: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        signView = findViewById(R.id.signView)
        resultView = findViewById(R.id.resultView)
        inputView = findViewById(R.id.inputView)
//        inputView.showSoftInputOnFocus = false
        clearView = findViewById(R.id.clearView)
        inputSting = ""

        findViewById<TextView>(R.id.Number0View).setOnClickListener { inputText("0")}
        findViewById<TextView>(R.id.Number1View).setOnClickListener { inputText("1")}
        findViewById<TextView>(R.id.Number2View).setOnClickListener { inputText("2")}
        findViewById<TextView>(R.id.Number3View).setOnClickListener { inputText("3")}
        findViewById<TextView>(R.id.Number4View).setOnClickListener { inputText("4")}
        findViewById<TextView>(R.id.Number5View).setOnClickListener { inputText("5")}
        findViewById<TextView>(R.id.Number6View).setOnClickListener { inputText("6")}
        findViewById<TextView>(R.id.Number7View).setOnClickListener { inputText("7")}
        findViewById<TextView>(R.id.Number8View).setOnClickListener { inputText("8")}
        findViewById<TextView>(R.id.Number9View).setOnClickListener { inputText("9")}
        findViewById<TextView>(R.id.NumberDotView).setOnClickListener { inputText(".")}

        findViewById<TextView>(R.id.NumberDivideView).setOnClickListener {

            inputToResult()
            action()
            setSign("/")
        }
        findViewById<TextView>(R.id.NumberMiltiplyView).setOnClickListener {

            inputToResult()
            action()
            setSign("*")
        }
        findViewById<TextView>(R.id.NumberPlusView).setOnClickListener {

            inputToResult()
            action()
            setSign("+")
        }
        findViewById<TextView>(R.id.NumberMinusView).setOnClickListener {

            inputToResult()
            action()
            setSign("-")
        }

        findViewById<TextView>(R.id.NumberEqualView).setOnClickListener {
            makeResult()
        }

        clearView.setOnClickListener { clear() }
    }

    fun inputText(str: String) {
        inputView.append(str)
    }

    fun setSign(str: String) {
        signView.text = str
    }

    fun clear() {
        inputView.setText("")
        signView.text = ""
        resultView.text = ""
    }

    fun inputToResult() {
        if (resultView.text == "") {
            resultView.text = inputView.text
            inputView.setText("")
        }

    }

    fun makeResult() {
        try {
            if (signView.text != "") {
                val conc =
                    resultView.text.toString() + signView.text.toString() + inputView.text.toString()
                val ex = ExpressionBuilder(conc).build()
                resultView.text = ""
                signView.text = ""
                val res = ex.evaluate()
                val resLong = res.toLong()
                if (res == resLong.toDouble())
                    inputView.setText(resLong.toString())
                else
                    inputView.setText(res.toString())

            }
        } catch(e: Exception) {
            Log.d("Error", "message: ${e.message}")
        }
    }

    fun action() {
        try {
            if (signView.text != "") {
                val conc =
                    resultView.text.toString() + signView.text.toString() + inputView.text.toString()
                val ex = ExpressionBuilder(conc).build()
                inputView.setText("")
                val res = ex.evaluate()
                val resLong = res.toLong()
                if (res == resLong.toDouble())
                    resultView.text = resLong.toString()
                else
                    resultView.text = res.toString()

            }
        } catch(e: Exception) {
            Log.d("Error", "message: ${e.message}")
        }
    }
}